package main

import (
	"context"
	"log"
	"os"

	pb "gitlab.com/remotejob/rest-grpc-market/template"
	"google.golang.org/grpc"
)

const (
	address     = "209.97.146.86:10000"
	defaultName = "AlexMazurov"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewMarkerServiceClient(conn)

	// Contact the server and print out its response.
	name := defaultName
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	r, err := c.CreateClient(context.Background(), &pb.TemplateRequest{Name: name})
	if err != nil {
		log.Fatalf("could not create: %v", err)
	}
	log.Printf("Client GET: %s", r.Message)

	r, err = c.MakePayment(context.Background(), &pb.TemplatePayment{Name: name, Amount: 2000})
	if err != nil {
		log.Fatalf("could not create: %v", err)
	}
	log.Printf("Client GET: %s", r.Message)

	r, err = c.Check(context.Background(), &pb.TemplateRequest{Name: name})
	if err != nil {
		log.Fatalf("could not create: %v", err)
	}
	log.Printf("Client GET: %s", r.Message)



	// r, err := c.SendGet(context.Background(), &pb.TemplateRequest{Name: name})
	// if err != nil {
	// 	log.Fatalf("could not greet: %v", err)
	// }
	// log.Printf("Client GET: %s", r.Message)

	// r, err = c.SendPost(context.Background(), &pb.TemplateRequest{Name: name})
	// if err != nil {
	// 	log.Fatalf("could not greet: %v", err)
	// }
	// log.Printf("Client POST: %s", r.Message)
}
