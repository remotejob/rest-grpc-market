package main

import (
	"context"
	"log"
	"net"
	"strconv"

	pb "gitlab.com/remotejob/rest-grpc-market/template"
	// "golang.org/x/net/context"
	"google.golang.org/grpc"
)

const (
	port = ":10000"
)

type server struct {
	db map[string]int
}

func (s *server) CreateClient(ctx context.Context, in *pb.TemplateRequest) (*pb.TemplateResponse, error) {

	s.db[in.GetName()] = 0

	return &pb.TemplateResponse{Message: "Client " + in.Name + " was created"}, nil
}

func (s *server) MakePayment(ctx context.Context, in *pb.TemplatePayment) (*pb.TemplateResponse, error) {

	name := in.GetName()
	amount := in.GetAmount()

	s.db[name] = int(amount)

	amountstr := strconv.FormatInt(int64(amount), 10)

	return &pb.TemplateResponse{Message: "Client get " + amountstr + " EUR"}, nil
}

func (s *server) Check(ctx context.Context, in *pb.TemplateRequest) (*pb.TemplateResponse, error) {

	var msg string

	if amountint, ok := s.db[in.GetName()]; ok {

		amount := strconv.Itoa(amountint)

		msg = "Client " + in.GetName() + " have " + amount + " EUR on his account"

	} else {
		
		msg = "Client " + in.GetName() + " not created!!!"

	}

	return &pb.TemplateResponse{Message: msg}, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	// pb.RegisterMarkerServiceServer(s, &server{})
	// Register reflection service on gRPC server.

	dbmap := make(map[string]int, 0)

	pb.RegisterMarkerServiceServer(s, &server{db: dbmap})
	s.Serve(lis)
	// reflection.Register(s)
	// if err := s.Serve(lis); err != nil {
	// 	log.Fatalf("failed to serve: %v", err)
	// }
}
