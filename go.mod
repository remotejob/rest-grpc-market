module gitlab.com/remotejob/rest-grpc-market

require (
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/protobuf v1.1.0
	github.com/grpc-ecosystem/grpc-gateway v1.4.1
	golang.org/x/net v0.0.0-20180808004115-f9ce57c11b24
	golang.org/x/sys v0.0.0-20180808154034-904bdc257025 // indirect
	golang.org/x/text v0.3.0 // indirect
	google.golang.org/genproto v0.0.0-20180808183934-383e8b2c3b9e
	google.golang.org/grpc v1.14.0
)
