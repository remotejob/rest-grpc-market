# rest-grpc-market

Цель проекта: осуществить плавный переход от REST к более продвинутой технлогии 

https://grpc.io/


Основой для проекта послужила статья

https://medium.com/@vptech/complexity-is-the-bane-of-every-software-engineer-e2878d0ad45a


Проект установлен на сервере http://209.97.146.86:8080/swagger/template.swagger.json



StartUP

```
curl 209.97.146.86:8080/swagger/template.swagger.json

curl -X POST 209.97.146.86:8080/create -d '{"name": "AlexMazurov"}'
curl -X POST 209.97.146.86:8080/payment -d '{"name": "AlexMazurov","amount": 2000}'
curl -X POST 209.97.146.86:8080/check -d '{"name": "AlexMazurov"}'

curl -X POST 209.97.146.86:8080/check -d '{"name": "Andrew"}'
curl -X POST 209.97.146.86:8080/create -d '{"name": "Andrew"}'
curl -X POST 209.97.146.86:8080/payment -d '{"name": "Andrew","amount": 5000}'
curl -X POST 209.97.146.86:8080/check -d '{"name": "Andrew"}'

```

check files:

https://gitlab.com/remotejob/rest-grpc-market/blob/master/template/template.proto

https://gitlab.com/remotejob/rest-grpc-market/blob/master/post.sh


Внимание:!!!
Для компиляции использовался GO1.11 !!! (Modules vs dep )


https://github.com/golang/go/wiki/Modules

https://gitlab.com/remotejob/rest-grpc-market/tree/master/bin


Client bin/grpcclient  

https://gitlab.com/remotejob/rest-grpc-market/blob/master/client/main.go


демонстрирует как работает  gRPC













